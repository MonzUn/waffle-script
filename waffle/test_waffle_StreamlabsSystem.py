# -*- coding: utf-8 -*-

# Copyright (C) 2020 pirak
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

import unittest
import datetime

import pytest

import waffle_StreamlabsSystem
from waffle.test_model import *
from waffle_StreamlabsSystem import *


class TestScript(unittest.TestCase):
    def setUp(self):
        waffle_StreamlabsSystem.Parent = Parent()
        self.Parent = waffle_StreamlabsSystem.Parent
        waffle_StreamlabsSystem.Init()

    def test_minutes_from_seconds(self):
        assert minutes_from_seconds(29) == 0
        assert minutes_from_seconds(30) == 1
        assert minutes_from_seconds(60) == 1
        assert minutes_from_seconds(60 + 29) == 1
        assert minutes_from_seconds(60 + 30) == 2
        assert minutes_from_seconds(7200 + 125) == 122

    def test_user_points_percent(self):
        assert user_points_percent("pirak__", 20) == 200
        assert user_points_percent("uneven_points", 20) == 19

    def test_has_mod_permission(self):
        assert not has_mod_permission("uneven_points")
        assert has_mod_permission("pirak__")
        assert has_mod_permission("a_mod")

    def test_parse_points(self):
        user = "pirak__"

        assert parse_points(user, "all") == 1000
        assert parse_points(user, "all") == self.Parent.GetPoints(user)

        for i in range(0, 101):
            assert parse_points(user, "{}%".format(i)) == i * 10

        with pytest.raises(ValueError):
            parse_points(user, "101%")


class IntegrationTest(unittest.TestCase):
    def setUp(self):
        waffle_StreamlabsSystem.Parent = Parent()
        self.Parent = waffle_StreamlabsSystem.Parent
        self.Settings = waffle_StreamlabsSystem.Settings()
        waffle_StreamlabsSystem.Init()

    def tearDown(self):
        self.Parent.message_cache = []
        waffle_StreamlabsSystem.reset_gamble_state()

    def _latest_msg(self):
        if len(self.Parent.message_cache) > 0:
            return self.Parent.message_cache.pop()
        else:
            return None

    def _start_time_backwards_by(self, minutes=15, seconds=0):
        delta = datetime.timedelta(minutes=minutes, seconds=seconds)
        if waffle_StreamlabsSystem.start_time is not None:
            waffle_StreamlabsSystem.start_time -= delta.total_seconds()

    def test_no_params(self):
        """
        Should be ignored
        """
        Execute(Data("anyone", "anyone", "!waffle"))
        assert self._latest_msg() is None

    def test_start_waffle(self):
        # should be ignored
        Execute(Data("anyone", "anyone", "!waffle start"))
        assert waffle_StreamlabsSystem.start_time is None
        assert self._latest_msg() is None

        Execute(Data("pirak__", "pirak__", "!waffle start"))
        assert waffle_StreamlabsSystem.start_time is not None
        assert self._latest_msg() == "Waffling started! {}".format(
            self.Settings.msg_correct_usage)

    def test_start_waffle_twice(self):
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        assert waffle_StreamlabsSystem.start_time is not None
        assert self._latest_msg() == "Waffling started! {}".format(
            self.Settings.msg_correct_usage)

        Execute(Data("pirak__", "pirak__", "!waffle start"))
        assert waffle_StreamlabsSystem.start_time is not None
        assert self._latest_msg() \
               == "Other waffling running currently! Cannot start!"

    def test_waffle_info(self):
        """
        Should only send something if round is running and caller has permissions
        """
        Execute(Data("pirak__", "pirak__", "!waffle"))
        assert self._latest_msg() is None

        Execute(Data("pirak__", "pirak__", "!waffle start"))
        self.Parent.message_cache.pop()

        Execute(Data("pirak__", "pirak__", "!waffle"))
        assert self._latest_msg() \
               == self.Settings.msg_current_waffling_time.format(0)

        Execute(Data("a_mod", "a_mod", "!waffle"))
        assert self._latest_msg() \
               == self.Settings.msg_current_waffling_time.format(0)

        Execute(Data("anyone", "anyone", "!waffle"))
        assert self._latest_msg() is None

        self._start_time_backwards_by(minutes=10, seconds=20)
        Execute(Data("a_mod", "a_mod", "!waffle"))
        assert self._latest_msg() \
               == self.Settings.msg_current_waffling_time.format(10)

    def test_enter_gamble_success(self):
        """
        Various correct ways of entering the gambling
        """
        user = "a_mod"
        Execute(Data(user, user, "!waffle start"))
        Execute(Data(user, user, "!waffle 45 60"))
        assert waffle_StreamlabsSystem.current_bet_amounts[user] == 60
        assert self.Parent.GetPoints(user) == 40

        user = "pirak__"
        Execute(Data(user, user, "!waffle 20 all"))
        assert waffle_StreamlabsSystem.current_bet_amounts[user] == 1000
        assert self.Parent.GetPoints(user) == 0

        user = "uneven_points"
        Execute(Data(user, user, "!waffle 25 50%"))
        assert waffle_StreamlabsSystem.current_bet_amounts[user] == 49
        assert self.Parent.GetPoints(user) == 50

    def test_enter_gamble_fail(self):
        """
        Various incorrect ways of entering the gambling
        """
        user = "a_mod"
        Execute(Data(user, user, "!waffle start"))

        Execute(Data(user, user, "!waffle 45 101"))
        assert user not in waffle_StreamlabsSystem.current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() \
               == self.Settings.msg_not_enough_points.format(user)

        Execute(Data(user, user, "!waffle 45 0"))
        assert user not in waffle_StreamlabsSystem.current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() \
               == self.Settings.min_gamble_info.format(self.Settings.min_gamble)

        Execute(Data(user, user, "!waffle 20 -1"))
        assert user not in waffle_StreamlabsSystem.current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() \
               == self.Settings.min_gamble_info.format(self.Settings.min_gamble)

        Execute(Data(user, user, "!waffle 20 allX"))
        assert user not in waffle_StreamlabsSystem.current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() == self.Settings.msg_correct_usage

        Execute(Data(user, user, "!waffle 25 101%"))
        assert user not in waffle_StreamlabsSystem.current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() == self.Settings.msg_correct_usage

        Execute(Data(user, user, "!waffle 25 -3%"))
        assert user not in waffle_StreamlabsSystem.current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() == self.Settings.msg_correct_usage

        Execute(Data(user, user, "!waffle -3 10%"))
        assert user not in waffle_StreamlabsSystem.current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() \
               == "Predicted minutes of waffling has to be > 0!"

        Execute(Data(user, user, "!waffle 0 10%"))
        assert user not in waffle_StreamlabsSystem.current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() \
               == "Predicted minutes of waffling has to be > 0!"

    def test_gamble_enter_twice(self):
        """
        Should only enter once and not overwrite old entry
        """
        user = "pirak__"
        Execute(Data(user, user, "!waffle start"))
        Execute(Data(user, user, "!waffle 45 60"))
        assert waffle_StreamlabsSystem.current_bet_amounts[user] == 60
        assert self.Parent.GetPoints(user) == 940

        Execute(Data(user, user, "!waffle 45 120"))
        assert waffle_StreamlabsSystem.current_bet_amounts[user] == 60
        assert self.Parent.GetPoints(user) == 940
        assert self._latest_msg() \
               == self.Settings.msg_already_gambled.format(user)

    def test_gamble_not_started(self):
        """
        Should fail to enter
        """
        user = "a_mod"
        Execute(Data(user, user, "!waffle 45 60"))
        assert waffle_StreamlabsSystem.start_time is None
        assert user not in waffle_StreamlabsSystem.current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert self._latest_msg() == self.Settings.msg_currently_no_waffling

    def test_bet_additional_params(self):
        """
        Should be ignored completely
        """
        user = "a_mod"
        Execute(Data(user, user, "!waffle start"))

        Execute(Data(user, user, "!waffle 45 60 50"))
        assert user not in waffle_StreamlabsSystem.current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert len(self.Parent.message_cache) == 1

        Execute(Data(user, user, "!waffle 45 err 50"))
        assert user not in waffle_StreamlabsSystem.current_bet_amounts.keys()
        assert self.Parent.GetPoints(user) == 100
        assert len(self.Parent.message_cache) == 1

    def test_cancel_invalid(self):
        """
        Should only be possibly by mod and if gambling has started
        """
        # no permission
        Execute(Data("anyone", "anyone", "!waffle cancel"))
        assert self._latest_msg() is None

        # not started
        Execute(Data("pirak__", "pirak__", "!waffle cancel"))
        assert self._latest_msg() is None

    def test_cancel_valid(self):
        """
        Should refund points to users
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))

        # still no permission
        Execute(Data("anyone", "anyone", "!waffle cancel"))
        assert self._latest_msg() \
               == "Waffling started! {}".format(self.Settings.msg_correct_usage)

        # let some users enter
        Execute(Data("pirak__", "pirak__", "!waffle 20 60"))
        assert self.Parent.GetPoints("pirak__") == 940
        Execute(Data("a_mod", "a_mod", "!waffle 43 20"))

        Execute(Data("pirak__", "pirak__", "!waffle cancel"))

        # check refunds
        assert self.Parent.GetPoints("pirak__") == 1000
        assert self.Parent.GetPoints("a_mod") == 100
        # check no longer running
        assert waffle_StreamlabsSystem.start_time is None

    def test_enter_after_closing(self):
        """
        Should not be possible
        """
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        self._start_time_backwards_by(minutes=self.Settings.entry_time + 2)
        Execute(Data("a_mod", "a_mod", "!waffle 20 40"))
        assert self._latest_msg() \
               == self.Settings.msg_no_longer_entry.format("a_mod")

    def test_stop_not_running(self):
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        assert self._latest_msg() \
               == "Cannot stop! Currently no waffling in progress!"

    def test_stop_no_enter_no_duration(self):
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        self._start_time_backwards_by()
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        assert self._latest_msg() == self.Settings.msg_no_win.format(15)

    def test_stop_no_enter_with_duration(self):
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        self._start_time_backwards_by(minutes=15)
        Execute(Data("pirak__", "pirak__", "!waffle stop 55"))
        assert self._latest_msg() == self.Settings.msg_no_win.format(55)

    def test_stop_no_enter_with_invalid_duration(self):
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        self._start_time_backwards_by(minutes=15)
        # should be ignored, waffling still running
        Execute(Data("pirak__", "pirak__", "!waffle stop -2"))
        assert len(self.Parent.message_cache) == 1
        Execute(Data("pirak__", "pirak__", "!waffle stop 5a"))
        assert len(self.Parent.message_cache) == 1
        assert waffle_StreamlabsSystem.start_time is not None

    def test_win_single(self):
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        Execute(Data("pirak__", "pirak__", "!waffle 40 20"))
        Execute(Data("a_mod", "a_mod", "!waffle 45 60"))
        self._start_time_backwards_by(minutes=46)
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        assert self._latest_msg() \
               == self.Settings.msg_win_single.format("a_mod", 1, 46, 80)

    def test_win_single_exact(self):
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        Execute(Data("pirak__", "pirak__", "!waffle 46 20"))
        Execute(Data("a_mod", "a_mod", "!waffle 40 60"))
        self._start_time_backwards_by(minutes=46)
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        assert self._latest_msg() \
               == self.Settings.msg_win_single.format("pirak__", 0, 46, 80 * 3)

    def test_win_multiple(self):
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        Execute(Data("uneven_points", "uneven_points", "!waffle 48 60"))
        Execute(Data("pirak__", "pirak__", "!waffle 45 20"))
        Execute(Data("a_mod", "a_mod", "!waffle 47 60"))
        self._start_time_backwards_by(minutes=46)
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        winners = "pirak__, a_mod"
        assert self._latest_msg() \
               == self.Settings.msg_win_multiple.format(winners, 1, 46, 70)

    def test_win_multiple_exact(self):
        Execute(Data("pirak__", "pirak__", "!waffle start"))
        Execute(Data("pirak__", "pirak__", "!waffle 46 20"))
        Execute(Data("a_mod", "a_mod", "!waffle 46 60"))
        self._start_time_backwards_by(minutes=46)
        Execute(Data("pirak__", "pirak__", "!waffle stop"))
        winners = "pirak__, a_mod"
        assert self._latest_msg() \
               == self.Settings.msg_win_multiple.format(winners, 0, 46, 40 * 3)

    def test_save_settings(self):
        waffle_StreamlabsSystem.my_settings.Save(waffle_StreamlabsSystem.settingsfile)
        waffle_StreamlabsSystem.my_settings = waffle_StreamlabsSystem.Settings()

        assert os.path.exists(waffle_StreamlabsSystem.settingsfile)
        os.remove(waffle_StreamlabsSystem.settingsfile)
        f2 = waffle_StreamlabsSystem.settingsfile.replace(".json", ".js")
        assert os.path.exists(f2)
        os.remove(f2)
