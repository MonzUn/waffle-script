# -*- coding: utf-8 -*-

# The following License only applies to everything at line 121 or below. The
# settings class (lines 42-120) has been adapted from castorr91. The original
# copyright applies.

# Copyright (C) 2020 pirak
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and
# limitations under the Licence.

################################################################################

import codecs
import ctypes
import json
import os
import re
import time

################################################################################

# Uncomment during development to silence Parent not found errors
# from waffle.test_model import Parent

ScriptName = "Waffle"
Website = "https://www.twitch.tv/pirak__"
Description = "Lets users bet on the duration of waffling"
Creator = "pirak"
Version = "0.4.3"

################################################################################
# Load/Store settings from Json-File
#
# Settings class adapted from castorr91's (https://www.twitch.tv/castorr91)
# Gambling Script.
# Available in the AnkhBot discord https://discord.gg/Xv9dzD9

settingsfile = "settings_waffle.json"
my_settings = None
MB_YES = 6


class Settings:
    """" Loads settings from file if file is found if not uses default values"""

    # The 'default' variable names need to match UI_Config
    def __init__(self, settingsfile=None):
        if settingsfile and os.path.isfile(settingsfile):
            with codecs.open(settingsfile, encoding='utf-8-sig', mode='r') as f:
                self.__dict__ = json.load(f, encoding='utf-8-sig')
        else:  # set variables if no custom settings file is found
            self.command = "!waffle"
            self.min_gamble = 10
            self.entry_time = 15
            self.mult_exact = 3
            self.msg_already_gambled = "{0}, you are already entered to bet this round!"
            self.msg_correct_usage = "Use !waffle <minutes> <bet_points> to bet."
            self.msg_currently_no_waffling = "Currently no waffling in progress!"
            self.msg_not_enough_points = "{0}, you do not have enough points to bet!"
            self.msg_win_single = "Waffling ended after {2} minutes! User {0} was closest with a prediction off by {1} min and won {3} points!"
            self.msg_win_multiple = "Waffling ended after {2} minutes! Users {0} were closest with a prediction off by {1} min and won {3} points each!"
            self.msg_no_win = "Waffling ended after {0} minutes! No user entered!"
            self.min_gamble_info = "You need to bet at least {0} points!"
            self.msg_current_waffling_time = "Currently waffling for {0} minutes!"
            self.msg_no_longer_entry = "{0}, entries for betting have already closed!"
            self.msg_cancel_gamble = "Cancelled the current waffling and returned points!"

    # Reload settings on save through UI
    def Reload(self, data):
        """Reload settings on save through UI"""
        self.__dict__ = json.loads(data, encoding='utf-8-sig')

    def Save(self, settingsfile):
        """ Save settings contained within to .json and .js settings files. """
        try:
            with codecs.open(settingsfile, encoding="utf-8-sig", mode="w+") as f:
                json.dump(self.__dict__, f, encoding="utf-8", ensure_ascii=False)
            with codecs.open(settingsfile.replace("json", "js"), encoding="utf-8-sig", mode="w+") as f:
                f.write("var settings = {0};".format(json.dumps(self.__dict__, encoding='utf-8', ensure_ascii=False)))
        except ValueError:
            Parent.Log(ScriptName, "Failed to save settings to file.")


def SetDefaults():
    """Set default settings function"""
    MessageBox = ctypes.windll.user32.MessageBoxW
    returnValue = MessageBox(0, u"You are about to reset the settings, "
                                "are you sure you want to continue?"
                             , u"Reset settings file?", 4)
    if returnValue == MB_YES:
        returnValue = MessageBox(0, u"Settings successfully restored to default values"
                                 , u"Reset complete!", 0)
        global my_settings
        Settings.Save(my_settings, settingsfile)


def ReloadSettings(jsonData):
    """Reload settings on pressing the save button"""
    global my_settings
    my_settings.Reload(jsonData)


def SaveSettings():
    """Save settings on pressing the save button"""
    Settings.Save(my_settings, settingsfile)


################################################################################
################################################################################
################################################################################
# Actual implementation


# global variables
waffle_jackpot = 0
# maps minutes -> Users : { int -> [str] }
current_bets = {}
# maps User -> points entered : { str -> int }
current_bet_amounts = {}
start_time = None
re_percent = re.compile(r"^(100|\d{1,2})%$")

################################################################################
# Required functions


def Init():
    global my_settings
    my_settings = Settings(settingsfile)
    return


def Execute(data):
    if not data.IsChatMessage() or not data.IsFromTwitch():
        return

    if data.GetParam(0).lower() != my_settings.command:
        return

    param_count = data.GetParamCount()

    # the command itself counts as param
    if param_count == 1:
        handle_info(data)
    elif param_count > 1 and data.GetParam(1).lower() in ["start", "stop"]:
        handle_start_stop(data)
    elif param_count > 1 and data.GetParam(1).lower() == "cancel":
        handle_cancel(data)
    elif param_count == 3:
        handle_gamble(data)


def Tick():
    return


################################################################################
# Custom functions

def minutes_from_seconds(secs):
    return int(round(abs(secs) / 60.0, 0))


def user_points_percent(user, percent):
    """
    Expects percent as value in range [0, 100]
    """
    return int(Parent.GetPoints(user) * (percent / 100.0))


def has_mod_permission(user):
    return Parent.HasPermission(user, "Caster", "") \
        or Parent.HasPermission(user, "Moderator", "")


def reset_gamble_state():
    global current_bets
    global current_bet_amounts
    global start_time
    global waffle_jackpot

    current_bets = {}
    current_bet_amounts = {}
    start_time = None
    waffle_jackpot = 0


def parse_points(user, arg):
    arg = arg.lower().strip()
    if arg == "all":
        return Parent.GetPoints(user)
    elif re_percent.match(arg) is not None:
        percent = int(arg[:-1])
        return user_points_percent(user, percent)
    else:
        return int(arg)


def award_points_callback(users_failed):
    """
    Parent.AddPointsAll() can not award points to users no longer in chat
    """
    if len(users_failed) > 0:
        Parent.SendStreamMessage("Could not give points to users {0} because they are currently not in chat.".format(', '.join(users_failed)))


def send_usage():
    Parent.SendStreamMessage(my_settings.msg_correct_usage)


def handle_info(data):
    """
    Show information about the current gambling round
    """
    if not has_mod_permission(data.User):
        return

    if start_time is None:
        return

    now = time.time()
    diff = minutes_from_seconds(now - start_time)
    Parent.SendStreamMessage(my_settings.msg_current_waffling_time.format(diff))


def handle_start_stop(data):
    global start_time

    if not has_mod_permission(data.User):
        return

    param = data.GetParam(1).lower()
    if param == "start":
        if start_time is not None:
            Parent.SendStreamMessage("Other waffling running currently! Cannot start!")
            return
        reset_gamble_state()
        start_time = time.time()
        Parent.SendStreamMessage("Waffling started! {}".format(my_settings.msg_correct_usage))
    elif param == "stop":
        handle_stop(data)


def handle_stop(data):
    """
    Stop the current gambling round and award points to winners
    """
    if start_time is None:
        Parent.SendStreamMessage("Cannot stop! Currently no waffling in progress!")
        return

    # `!command stop` OR `!command stop duration`
    if data.GetParamCount() == 2:
        stop_time = time.time()
        diff_minutes = minutes_from_seconds(stop_time - start_time)
    elif data.GetParamCount() == 3:
        try:
            diff_minutes = int(data.GetParam(2))
            if diff_minutes < 0:
                raise ValueError
        except ValueError:
            return

    closest = None
    for prediction, user_list in current_bets.items():
        diff = abs(prediction - diff_minutes)
        if closest is None or diff < closest[0]:
            closest = (diff, user_list)
        # might happen for e.g. actual=2 and existing bets 1 and 3
        elif diff == closest[0]:
            closest[1].extend(user_list)

    if closest is None:
        Parent.SendStreamMessage(my_settings.msg_no_win.format(diff_minutes))
        return

    handle_wins(closest[1], diff_minutes, closest[0])
    reset_gamble_state()


def handle_wins(users, actual_time, diff_actual):
    """
    Award points to winners
    """
    points_per_user = int(round(waffle_jackpot / len(users), 0))
    if diff_actual == 0:
        points_per_user = points_per_user * my_settings.mult_exact

    points_map = {}
    for u in users:
        points_map[u] = points_per_user
    Parent.AddPointsAllAsync(points_map, award_points_callback)

    message = None
    if len(users) == 1:
        message = my_settings.msg_win_single.format(users[0], diff_actual, actual_time, points_per_user)
    else:
        message = my_settings.msg_win_multiple.format(', '.join(users), diff_actual, actual_time, points_per_user)

    Parent.SendStreamMessage(message)


def handle_cancel(data):
    """
    Cancel a running betting round and return points
    """
    if not has_mod_permission(data.User):
        return

    if start_time is None:
        return

    Parent.AddPointsAllAsync(current_bet_amounts, award_points_callback)
    Parent.SendStreamMessage(my_settings.msg_cancel_gamble)
    reset_gamble_state()


def handle_gamble(data):
    """
    Handle users entering the gambling
    """
    global current_bets
    global current_bet_amounts
    global waffle_jackpot

    # make sure waffling is in progress
    if start_time is None:
        Parent.SendStreamMessage(my_settings.msg_currently_no_waffling)
        return

    # make sure the user cannot enter twice
    if any(map(lambda users_item: data.User in users_item[1], current_bets.items())):
        Parent.SendStreamMessage(my_settings.msg_already_gambled.format(data.UserName))
        return

    # make sure the user cannot enter after the closing time
    now = time.time()
    diff = minutes_from_seconds(now - start_time)
    if diff > my_settings.entry_time:
        Parent.SendStreamMessage(my_settings.msg_no_longer_entry.format(data.UserName))
        return

    # all preconditions checked: now actual gamble entry handling is possible
    minutes = None
    gamble_amount = None

    try:
        minutes = int(data.GetParam(1))
        # gamble_amount = int(data.GetParam(2))
        gamble_amount = parse_points(data.User, data.GetParam(2))
    except ValueError:
        send_usage()
        return

    if minutes <= 0:
        Parent.SendStreamMessage("Predicted minutes of waffling has to be > 0!")
        return

    if gamble_amount < my_settings.min_gamble:
        Parent.SendStreamMessage(my_settings.min_gamble_info.format(my_settings.min_gamble))
        return

    # make sure the user has enough points
    if gamble_amount > Parent.GetPoints(data.User):
        Parent.SendStreamMessage(my_settings.msg_not_enough_points.format(data.UserName))
        return

    Parent.RemovePoints(data.User, data.UserName, gamble_amount)
    waffle_jackpot += gamble_amount

    users = current_bets.get(minutes, [])
    users.append(data.User)
    current_bets[minutes] = users
    current_bet_amounts[data.User] = gamble_amount
