# Waffling Script

Description: Streamlabs-Chatbot script that lets users bet on the duration of waffling    
Creator: pirak    
The latest ready-to-use package can be found on the
[Releases Page](https://gitlab.com/pirak/waffle-script/-/releases)
in the ‘Other’ section of the newest release as `waffle-vX.Y.Z.zip`.

Lets users bet on how long the streamer is going to waffle before starting a
game. The bet is given in minutes. Users will have to use a configurable minimum
amount of points in their bet.
A time can be configured after which entries are no longer possible to avoid
users enter just before stopping the bet.

If only one user is closest to the actual time, then they receive the whole
pot. If multiple people are equally close, the pot is split equally among all
winning users. If the predicted time matched the actual one exactly a multiplier
to the won points can be applied.


## Usage
- Users: `!waffle <minutes> <bet_points>`
  - Examples:
    - `!waffle 45 100` to bet on a waffling duration of 45 minutes while
      entering 100 points into the jackpot.
    - `!waffle 45 50%` for the user the gamble 50% of their total points
    - `!waffle 45 all` for the use to gamble all of their points
- Only Caster/Mods:
  - `!waffle start`
  - `!waffle stop` or `!waffle stop <duration>`
    - to stop the waffling either with a calculated time difference since start
    - or by giving a specific time difference in minutes
  - `!waffle cancel` to stop an in-progress waffling returning points to users
  - `!waffle` to show the current waffling duration


## Dev-Stuff
[![pipeline status](https://gitlab.com/pirak/waffle-script/badges/main/pipeline.svg)](https://gitlab.com/pirak/waffle-script/-/commits/main)
[![coverage report](https://gitlab.com/pirak/waffle-script/badges/main/coverage.svg)](https://gitlab.com/pirak/waffle-script/-/commits/main)

- Zip the `waffle` directory to create an importable script into the
  Streamlabs chatbot.
- Some documentation on the `Parent`-Object:
  https://github.com/AnkhHeart/Streamlabs-Chatbot-Python-Boilerplate/wiki/Parent
- The Streamlabs-Chatbot still only supports Python 2.


# License

## Adapted Code
The script contains the `Settings` class adapted from castorr91
(https://www.twitch.tv/castorr91) made originally available in the AnkhBot
Discord https://discord.gg/Xv9dzD9 as part of their Gambling script.
The original copyright applies.
The adapted sections are annotated accordingly in the script.

## Own Code
Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
the European Commission - subsequent versions of the EUPL (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:
https://joinup.ec.europa.eu/software/page/eupl

Unless required by applicable law or agreed to in writing, software
distributed under the Licence is distributed on an "AS IS" basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the Licence for the specific language governing permissions and
limitations under the Licence.
